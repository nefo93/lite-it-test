<?php
class translate {
    protected $str;
    protected $array = [1000 => 'M',900=>"CM",500=>"D",400=>"CD",100=>"C", 90=>"XC",50=>"L",40=>"XL",10=>"X", 9=>"IX",5=>"V",4=>"IV",1=>"I"];

    function __construct($str) {
        $this->str = $str;
    }

    function roman() {
        if( $this->str < 0 ) return "";
        if( !$this->str ) return "0";
        $thousands = (int)( $this->str / 1000 );
        $this->str -= $thousands*1000;
        $result = str_repeat("M",$thousands);
        while ( $this->str ) {
            foreach ( $this->array as $part => $fragment ) if( $part <= $this->str ) break;
            $amount = (int)($this->str / $part);
            $this->str -= $part*$amount;
            $result .= str_repeat($fragment,$amount);
        }
        return $result;
    }
    function arab() {
        if( $this->str == '' ) return '';
        if( !$this->str ) return '0';
        $countstr = strlen($this->str);
        $result = 0;
        for($i = 0; $i < $countstr; $i++) {
            $str = $this->str[$i];
            foreach ( $this->array as $part => $fragment ) {
                if($str == $fragment) {
                    $result += $part;
                    break;
                }
            }
        }
        return $result;
    }

}

if($_POST) {
//    $b = json_encode(['text' => $_POST['text'], 'leng' => $_POST['language']]);
//    echo $b;
$text = $_POST['text'];
$language = $_POST['language'];
$trans = new translate($text);
    if($language == 'Rome') {
        $result = $trans->roman();
    }else {
        $result = $trans->arab();
    }

    if($result) {
        echo $result;
    } else {
       echo 'Error';
    }

}