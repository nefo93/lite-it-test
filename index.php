<?php
require_once 'function.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="asset/css/style.css">
    <script src="asset/js/jquery-3.3.1.min.js"></script>
</head>
<body>
<div class="container">
    <div class="d-flex flex-wrap">
        <select id="language" class="form-control">
            <option>Arab</option>
            <option>Rome</option>
        </select>
        <div class="text">
            <textarea class="form-control" rows="5" id="text"></textarea>
        </div>
        <button id="sub"><img src="asset/img/right-arrow.png"></button>
        <div class="block form-control">
            <p id="result"></p>
        </div>
    </div>
</div>

<script>
    $('#sub').on('click', function () {
        var language = $('#language').val();
        var text = $('#text').val();
       $.ajax({
           url: "function.php",
           type: "POST",
           data: ({language, text}),
           success:function (data) {
               $('#result').text(data);
           }
       });
    });
</script>
</body>
</html>